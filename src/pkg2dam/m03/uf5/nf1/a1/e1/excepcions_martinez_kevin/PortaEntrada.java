/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dam.m03.uf5.nf1.a1.e1.excepcions_martinez_kevin;

/**
 *
 * @author Azurale
 */
public class PortaEntrada {
    private final static int MASSA_BAIX = 150;
    private final static int MASSA_ALT = 190;
    
    public void comprovarAlcada(Sensor s1) throws AlcadaException {
        int alcada = s1.obtenirAlcada();
        System.out.printf("SENSOR: Alçada llegida = %d%n", alcada);
        if(alcada < MASSA_BAIX){
            throw new PocaAlcadaException("no arriba a l'alçada mínima de 150");
        }else if (alcada > MASSA_ALT) {
            throw new PocaAlcadaException("alçada màxima de 190 sobrepassada");
        }else{
            System.out.printf("Obrint la porta a l'alçada %d cms%n", alcada);
        }
    }
}
