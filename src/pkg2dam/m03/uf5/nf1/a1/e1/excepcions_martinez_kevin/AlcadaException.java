/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dam.m03.uf5.nf1.a1.e1.excepcions_martinez_kevin;

/**
 *
 * @author Azurale
 */
public class AlcadaException extends Exception {
    
    public AlcadaException() {
        super();
    }

    public AlcadaException(String msg) {
        super(msg);
    }
    
}
